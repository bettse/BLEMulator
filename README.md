# BLEMulator

This repo started as a fork of [BlueSim](https://github.com/AttackPattern/BlueSim), an iOS app that can emulate various BLE device.

I've also included an arduino project using a BlendMicro (Arudino + BLE) that acts as a BLE portal emulator and token emulator in the vportal folder.

## Functionality

I've added the following features, but the ones towards the end of the list are older and may have become broken.

 * Emulate BLE Portal
 * Emulate adding and removing virtual token
 * Store/read tokens from REST backend
 * Connect to real portal
 * Dump tokens place on real portal
 * Proxy communication between game and real portal
 * Use template token to iterate and test specific values in token data format.


## Video

Check out [this video](BLEMulator.mkv) to see it in action.  Made with [Reflector](http://www.airsquirrels.com/reflector/).

## Normal use case

![](DeviceInterface.png)

This app can replace all the items above the Bluetooth logo.



