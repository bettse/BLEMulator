
// Import libraries (BLEPeripheral depends on SPI)
#include <SPI.h>
#include <BLEPeripheral.h>
#include <dataflash.h>
#include <Bounce2.h>
#include "VirtualPortal.h"
#include "NavSwitch.h"
#include <MemoryFree.h>

// define pins (varies per shield/board)
#define BLE_REQ   6
#define BLE_RDY   7
#define BLE_RST   4

//serLCD
#define LCD_CMD_1 0xFE
#define LCD_CMD_2 0x7C
#define BACKLIGHT_BASE 0x80
#define BACKLIGHT_LEVELS 30

NavSwitch nav = NavSwitch(12, 11, 10, 9);

// create peripheral instance, see pinouts above
BLEPeripheral blePeripheral = BLEPeripheral(BLE_REQ, BLE_RDY, BLE_RST);

// create one or more services
BLEService service = BLEService("533E15303ABEF33FCD00594E8B0A8EA3");
BLEService shortService = BLEService("1530");

// create one or more characteristics
BLECharacteristic txCharacteristic = BLECharacteristic("533E15423ABEF33FCD00594E8B0A8EA3", BLERead | BLENotify, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
BLECharacteristic rxCharacteristic = BLECharacteristic("533E15433ABEF33FCD00594E8B0A8EA3", BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);

VirtualPortal vp = VirtualPortal();

unsigned char len = 0;

long previousMillis = 0;        // will store last time LED was updated
long interval = 1000;           // interval at which to blink (milliseconds)
bool subscribed = false;

int libraryId = 0; //Token being displayed
int previousId = -1; //track changes without constant redrawing, -1 to cause LCD draw on first load

Token *next = NULL;
unsigned int token_count = 199;

void setup() {
    Serial.begin(115200);
    delay(3000);  //3 seconds delay for enabling to see the start up comments on the serial board

    LCD.begin(9600);
    LCD.write(LCD_CMD_1);   //command flag
    LCD.write(0x01);   //clear command.
    //TODO: Turn the LCD down x seconds after last user interaction
    LCD.write(LCD_CMD_2);
    LCD.write(BACKLIGHT_BASE + BACKLIGHT_LEVELS / 2);

    nav.init();

    blePeripheral.setDeviceName("Skylanders Portal\0");
    blePeripheral.setLocalName("Skylanders Portal\0");
    blePeripheral.setAdvertisedServiceUuid(shortService.uuid());

    // add attributes (services, characteristics, descriptors) to peripheral
    blePeripheral.addAttribute(service);
    blePeripheral.addAttribute(rxCharacteristic);
    blePeripheral.addAttribute(txCharacteristic);

    blePeripheral.setEventHandler(BLEConnected, connectCallback);
    blePeripheral.setEventHandler(BLEDisconnected, disconnectCallback);
    txCharacteristic.setEventHandler(BLESubscribed, subscribeHandler);
    txCharacteristic.setEventHandler(BLEUnsubscribed, unsubscribeHandler);
    rxCharacteristic.setEventHandler(BLEWritten, writeHandler);

    // begin initialization
    blePeripheral.begin();
}

/* The general order is
 * 1. BLE
 * 2. updates based on interval
 * 3. updates based on user
 * 4. updates to UI
**/

void loop() {
  blePeripheral.poll();
  int update = nav.update();
  unsigned long currentMillis = millis();

  if(ble_busy()) { return; }

  //Do something every interval
  if(currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;

    if (next) {
      vp.loadToken(next);
      next = NULL;
    }

    //1073 during my last check
    //Serial.print("freeMemory()="); Serial.println(freeMemory());
  }

  if (Serial.available()) {
    int cmd = Serial.parseInt();
    if (cmd == 0) {
      next = new Token(libraryId);
    } else {
      libraryId = cmd;
      libraryId = positive_modulo(libraryId, token_count);
    }
  }

  //Look for navigation
  if (update) {
    NavSwitch::NavDir direction = nav.read();
    switch (direction) {
      case NavSwitch::ONE:
        libraryId = libraryId++;
        break;
      case NavSwitch::TWO:
        libraryId = libraryId--;
        break;
      case NavSwitch::TEE: //Select
        next = new Token(libraryId);
        break;
    } //end switch
    libraryId = positive_modulo(libraryId, token_count);
  }//end update

  if (next) {
    next->display();
    vp.removeType(next->type());
  } else if (libraryId != previousId) {
    previousId = libraryId;
    Serial.print(libraryId, DEC); Serial.print(": ");
    Token preview(libraryId);
    preview.display();
  }

}

// callbacks
void connectCallback(BLECentral& central) {
  Serial.println(F("Connected"));
  vp.connect();
}

void disconnectCallback(BLECentral& central)
{
    vp.disconnect();
    subscribed = false;
}

void subscribeHandler(BLECentral& central, BLECharacteristic& characteristic)
{
  Serial.println(F("Subscribed"));
  vp.subscribe();
  subscribed = true;
}

void unsubscribeHandler(BLECentral& central, BLECharacteristic& characteristic)
{
  vp.unsubscribe();
  subscribed = false;
}

void writeHandler(BLECentral& central, BLECharacteristic& characteristic)
{
    unsigned char len = characteristic.valueLength();
    uint8_t *val = (uint8_t*)characteristic.value();
    uint8_t response[BLE_ATTRIBUTE_MAX_VALUE_LENGTH] = {0};
    unsigned long start = millis();
    len = vp.respondTo(val, response);

    //respond if data to respond with
    if (len > 0) {
      txCharacteristic.setValue(response, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
    }
}

unsigned char ble_busy() {
    return (digitalRead(BLE_REQ) == LOW);
}

inline int positive_modulo(int i, int n) {
  return (i % n + n) % n;
}

