//
//  InterfaceController.h
//  BlueSim WatchKit Extension
//
//  Created by Eric Betts on 3/14/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
