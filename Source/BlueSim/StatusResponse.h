//
//  StatusResponse.h
//  BlueSim
//
//  Created by Eric Betts on 2/13/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BLE_SIZE 20

#define b00 0x0
#define b01 0x1
#define b10 0x2
#define b11 0x3

#define COMING @"↓"
#define GOING @"↑"
#define FULL @"."
#define EMPTY @" "

@interface StatusResponse : NSObject {
}

- (StatusResponse *)initWithData:(NSData *)data;
@end
