//
//  NSString+dataFromHex.h
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (dataFromHex)

-(NSData *) hexData;

@end
