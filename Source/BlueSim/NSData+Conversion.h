//
//  NSData+Conversion.h
//  BlueSim
//
//  Created by Eric Betts on 12/19/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Conversion)

#pragma mark - String Conversion
- (NSString *)hexadecimalString;

@end
