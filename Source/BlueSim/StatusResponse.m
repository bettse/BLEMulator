//
//  StatusResponse.m
//  BlueSim
//
//  Created by Eric Betts on 2/13/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import "StatusResponse.h"

@implementation StatusResponse

- (StatusResponse *)initWithData:(NSData *)data {
    self = [super init];
    if (self) {
    }
    return self;
}

- (NSString *)tokenState:(uint8_t)stateBits {
    switch (stateBits) {
        case b00:
            return EMPTY;
            break;
        case b01:
            return FULL;
            break;
        case b10:
            return GOING;
            break;
        case b11:
            return COMING;
            break;
    }
    return @"";
}

@end
