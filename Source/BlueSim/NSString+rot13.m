//
//  NSString+rot13.m
//  BlueSim
//
//  Created by Eric Betts on 2/10/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import "NSString+rot13.h"

@implementation NSString (rot13)

-(NSString *) rot13 {
    const char *bytes = [self cStringUsingEncoding:NSASCIIStringEncoding];
    NSUInteger stringLength = [self length];
    char newString[stringLength+1];
    newString[stringLength] = '\0';
    
    for(int x = 0; x < stringLength; x++ ) {
        char aCharacter = bytes[x];
        if( 0x40 < aCharacter && aCharacter < 0x5B ) // A - Z
            newString[x] = (((aCharacter - 0x41) + 0x0D) % 0x1A) + 0x41;
        else if( 0x60 < aCharacter && aCharacter < 0x7B ) // a-z
            newString[x] = (((aCharacter - 0x61) + 0x0D) % 0x1A) + 0x61;
        else  // Not an alpha character
            newString[x] = aCharacter;
    }

    return [NSString stringWithCString:newString encoding:NSASCIIStringEncoding];
}

@end
