//
//  NSData+empty.m
//  BlueSim
//
//  Created by Eric Betts on 3/22/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import "NSData+empty.h"

@implementation NSData (empty)

- (BOOL)isEmpty {
    uint8_t zeros[MAX_ZEROS] = {0};
    if (self.length > MAX_ZEROS) {
        return NO; //This is only coded to support up to MAX_ZEROS (16);
    }
    return [self isEqual:[NSData dataWithBytes:zeros length:MAX_ZEROS]];
}

@end
