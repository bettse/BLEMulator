//
//  NSData+crc16Checksum.h
//  BlueSim
//
//  Created by Eric Betts on 12/20/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (crc16Checksum)

- (unsigned short)crc16Checksum;

@end
