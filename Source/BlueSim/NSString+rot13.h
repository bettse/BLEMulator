//
//  NSString+rot13.h
//  BlueSim
//
//  Created by Eric Betts on 2/10/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (rot13)
-(NSString *) rot13;
@end
