//
//  Token.m
//  BlueSim
//
//  Created by Eric Betts on 12/18/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "Token.h"

NSString *const allTokensRoute = @"allToken";

@implementation Token

- (Token *)initWithName:(NSString *)name andData:(NSData *)data {
    self = [super init];
    if (self) {
        self.name = name;
        self.data = [data hexadecimalString];
    }
    return self;
}

- (void)setData:(NSString *)data {
    if ([data length] < TOKEN_SIZE) {
        encodeddata = [[[self blankToken:data] hexData] mutableCopy];
    } else {
        encodeddata = [[data hexData] mutableCopy];
    }
    [self updateCRC];
}

- (NSString *)data {
    return [encodeddata hexadecimalString];
}

//BE CAREFUL!!!
- (void)reset {
    self.data = [self.data substringWithRange:NSMakeRange(0, 64)]; //64 hex bytes = 32 bytes = first 2 blocks
}

- (UIImage *)image {
    NSString *typeBytes = [[encodeddata subdataWithRange:NSMakeRange(16, 2)] hexadecimalString];
    NSString *variantBytes = [[encodeddata subdataWithRange:NSMakeRange(28, 2)] hexadecimalString];
    UIImage *elementImage = [UIImage imageNamed:self.element];

    if (self.categoryId == TRAP_CATID) { //Look for villain image
        NSString *filename = [[NSString stringWithFormat:@"0x%02x", [self villainId]] lowercaseString];
        UIImage *villainImage = [UIImage imageNamed:filename];
        if (villainImage) {
            return villainImage;
        } else {
            NSString *filename = [[NSString stringWithFormat:@"%@-%@", typeBytes, variantBytes] lowercaseString];
            UIImage *trapImage = [UIImage imageNamed:filename];
            if (trapImage) {
                return trapImage;
            }
        }
    } else {
        NSString *filename = [[NSString stringWithFormat:@"%@-%@", typeBytes, variantBytes] lowercaseString];
        UIImage *image = [UIImage imageNamed:filename];
        if (image) {
            return image;
        }
    }

    if (elementImage) {
        return elementImage;
    }
    return [UIImage imageNamed:@"SkyPortal@2x"];
}

//Make a second method for comlementary background color?
- (UIColor *)color {
    NSDictionary *elementColors = @{
        @"magic" : [UIColor purpleColor],
        @"earth" : [UIColor brownColor],
        @"water" : [UIColor blueColor],
        @"fire" : [UIColor redColor],
        @"tech" : [UIColor orangeColor],
        @"undead" : [UIColor grayColor],
        @"life" : [UIColor greenColor],
        @"air" : [UIColor cyanColor],
        @"dark" : [UIColor blackColor],
        @"light" : [UIColor yellowColor],
        @"none" : [UIColor whiteColor],
    };
    return [elementColors valueForKey:self.element];
}

- (NSString *)category {
    NSArray *characterClasses = @[ @"regular", @"mini", @"trapmaster", @"giant" ];
    if ([characterClasses containsObject:self.type]) {
        return @"character";
    }
    return self.type;
}

- (NSUInteger)categoryId {
    return [self.class.categories indexOfObject:self.category];
}

- (NSString *)blankToken:(NSString *)intro {
    NSMutableString *newToken = [intro mutableCopy];
    //'template' of tokens
    NSString *zeros = @"00000000000000000000000000000000";
    NSString *ro = @"0000000000000F0F0F69000000000000";
    NSString *rw = @"0000000000007F0F0869000000000000";
    for (int i = 2; i < BLOCK_COUNT; i++) {
        if (i == 3) {
            [newToken appendString:ro];
        } else if (i % 4 == 3) {
            [newToken appendString:rw];
        } else {
            [newToken appendString:zeros];
        }
    }
    return newToken;
}

- (void)updateCRC {
    //Replace CRC of blocks 0 and 1 if its not correct
    NSRange crcPos = NSMakeRange(30, 2);
    NSData *currentChecksum = [encodeddata subdataWithRange:crcPos];
    unsigned short chk = [[encodeddata subdataWithRange:NSMakeRange(0, crcPos.location)] crc16Checksum];
    NSData *calculatedChecksum = [NSData dataWithBytes:&chk length:crcPos.length];
    if (![currentChecksum isEqualToData:calculatedChecksum]) {
        //NSLog(@"Replacing CRC of blocks 0+1 (%@ -> %@)", currentChecksum, calculatedChecksum);
        [encodeddata replaceBytesInRange:crcPos withBytes:[calculatedChecksum bytes]];
    }
}

- (void)headerChecksum {
    uint8_t recentArea = [self recentArea];
    NSData *originalHeader = [self translate:recentArea];
    NSMutableData *modifiedHeader = [originalHeader mutableCopy];
    uint16_t placeholder = 0x0005;
    NSRange crcRange = NSMakeRange(BLOCK_SIZE - 2, 2);

    //Sub the existing CRC with 0x0005
    [modifiedHeader replaceBytesInRange:crcRange withBytes:&placeholder];

    uint16_t chk = [modifiedHeader crc16Checksum];
    [modifiedHeader replaceBytesInRange:crcRange withBytes:&chk];
    NSData *key = [self generateBlockKey:recentArea];
    NSData *encyphered = [self obscure:modifiedHeader usingKey:key];

    //NSLog(@"Precrypt:   %@ vs %@", originalHeader, modifiedHeader);
    //NSLog(@"Postcrypt:  %@ vs %@", [self readBlock:recentArea], encyphered);

    [self writeBlock:recentArea withData:encyphered];
}

- (NSData *)readBlock:(NSInteger)block {
    return [encodeddata subdataWithRange:NSMakeRange(block * BLOCK_SIZE, BLOCK_SIZE)];
}

- (void)writeBlock:(NSInteger)block withData:(NSData *)newData {
    [encodeddata replaceBytesInRange:NSMakeRange(block * BLOCK_SIZE, BLOCK_SIZE) withBytes:[newData bytes]];
}

- (uint16_t)typeId {
    unsigned short value = 0;
    [[self readBlock:1] getBytes:&value range:NSMakeRange(0, 2)];
    return value;
}

- (uint16_t)variantId {
    uint16_t value = 0;
    [[self readBlock:1] getBytes:&value range:NSMakeRange(12, 2)];
    return value;
}

- (NSUInteger)exp {
    unsigned int value = 0;
    if (self.categoryId == CHARACTER_CATID) {
        NSData *blockData = [self translate:[self recentAreaPlus:0]];
        [blockData getBytes:&value range:NSMakeRange(0, 3)];
    }
    return value;
}

- (uint16_t)money {
    uint16_t value = 0;
    if (self.categoryId == CHARACTER_CATID) {
        NSData *blockData = [self translate:[self recentAreaPlus:0]];
        [blockData getBytes:&value range:NSMakeRange(3, 2)];
    }
    return value;
}

- (void)setMoney:(uint16_t)value {
    if (self.categoryId != CHARACTER_CATID) {
        return;
    }
    uint8_t recentArea = [self recentArea];
    NSMutableData *headerBlock = [[self translate:recentArea] mutableCopy];
    [headerBlock replaceBytesInRange:NSMakeRange(3, 2) withBytes:&value];

    NSData *key = [self generateBlockKey:recentArea];
    NSData *encyphered = [self obscure:headerBlock usingKey:key];

    [self writeBlock:recentArea withData:encyphered];
    [self headerChecksum]; //recalculate checksums and re-encrypt
    NSLog(@"set money to %u, its now %u", value, self.money);
}

//Skills given by Fairy. Bit 7 = path chosen. FD0F = Left, FF0F = Right
- (NSUInteger)skills { //00001010
    unsigned int value = 0;
    NSData *blockData = [self translate:[self recentAreaPlus:1]];
    [blockData getBytes:&value range:NSMakeRange(0, 4)];
    return value;
}

- (uint16_t)hatId {
    uint8_t value = 0;
    NSData *blockData = [self translate:[self recentAreaPlus:9]];
    [blockData getBytes:&value range:NSMakeRange(12, 1)];
    return value;
}

- (uint8_t)villainId {
    uint8_t value = 0;
    if (self.categoryId == TRAP_CATID) {
        NSData *blockData = [self translate:[self recentAreaPlus:1]];
        [blockData getBytes:&value range:NSMakeRange(0, 1)];
    }
    return value;
}

//Consider making this return an array of villain chunks
- (NSUInteger)trappedVillains {
    uint16_t value = 0;
    NSData *blockData = [self translate:[self recentAreaPlus:0]];
    [blockData getBytes:&value range:NSMakeRange(0, 2)];
    return value;
}

- (BOOL)evolved {
    uint8_t value = 0;
    NSData *blockData = [self translate:[self recentAreaPlus:1]];
    [blockData getBytes:&value range:NSMakeRange(0, 1)];
    return (value == 1);
}

- (uint8_t)AreaASequence {
    uint8_t value = 0;
    [[self translate:0x08] getBytes:&value range:NSMakeRange(9, 1)];
    return value;
}

- (uint8_t)AreaBSequence {
    uint8_t value = 0;
    [[self translate:0x24] getBytes:&value range:NSMakeRange(9, 1)];
    return value;
}

- (uint8_t)recentAreaPlus:(uint8_t)offset {
    return [self recentArea] + offset;
}

- (uint8_t)recentArea {
    uint8_t a = [self AreaASequence];
    uint8_t b = [self AreaBSequence];
    return (a > b) ? 0x08 : 0x24;
}

- (NSString *)nickname {
    //This is different for villains in traps.  They have a nickname starting at 0x04 of chunk of data
    if (self.categoryId == CHARACTER_CATID) {
        NSData *blockDataFirst = [self translate:[self recentAreaPlus:2]];
        NSData *blockDataSecond = [self translate:[self recentAreaPlus:4]];

        NSString *first = [[NSString alloc] initWithData:blockDataFirst encoding:NSUTF16LittleEndianStringEncoding];
        NSString *second = [[NSString alloc] initWithData:blockDataSecond encoding:NSUTF16LittleEndianStringEncoding];

        return [NSString stringWithFormat:@"%@%@", first, second];
    } else if (self.categoryId == TRAP_CATID) {
        NSData *blockDataFirst = [self translate:[self recentAreaPlus:1]];
        NSData *blockDataSecond = [self translate:[self recentAreaPlus:2]];
        NSData *nameData = [blockDataFirst subdataWithRange:NSMakeRange(4, BLOCK_SIZE - 4)];

        NSString *first = [[NSString alloc] initWithData:nameData encoding:NSUTF16LittleEndianStringEncoding];
        NSString *second = [[NSString alloc] initWithData:blockDataSecond encoding:NSUTF16LittleEndianStringEncoding];

        return [NSString stringWithFormat:@"%@%@", first, second];
    }
    return @"";
}

- (void)dump {
    NSLog(@"A: %u / B: %u", [self AreaASequence], [self AreaBSequence]);
    for (int i = 0; i < 64; i++) {
        if ((i + 1) % 4 > 0) {
            NSData *blockData = [self translate:i];
            if (![blockData isEmpty]) {
                NSLog(@"[%02x] %@", i, blockData);
            }
        }
    }
}

- (NSString *)description {
    switch (self.categoryId) {
        case CHARACTER_CATID:
            break;
            return [NSString stringWithFormat:@"<Token %@: $%u exp:%lu hat:%x>", self.name, self.money, self.exp, self.hatId];
        case TRAP_CATID:
            return [NSString stringWithFormat:@"<Token %@: %02x %@>", self.name, [self villainId], self.nickname];
            break;
        //case ITEM_CATID:
        //case LOCATION_CATID:
    }
return [NSString stringWithFormat:@"<Token %@>", self.name];
}

- (void)save:(void (^)(void))successCallback {
    if (self._id) {
        [[RKObjectManager sharedManager] putObject:self path:nil parameters:nil
                                           success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                               successCallback();
                                           }
                                           failure:nil];
    } else {
        [[RKObjectManager sharedManager] postObject:self path:nil parameters:nil
                                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                successCallback();
                                            }
                                            failure:nil];
    }
}

- (NSComparisonResult)compare:(Token *)otherObject {
    if (self.typeId < otherObject.typeId) {
        return (NSComparisonResult)NSOrderedAscending;
    } else if (self.typeId > otherObject.typeId) {
        return (NSComparisonResult)NSOrderedDescending;
    }
    return (NSComparisonResult)NSOrderedSame;
}

- (NSData *)translate:(NSInteger)block {
    NSData *data = [self readBlock:block];
    if (block < 8 || (block % 4 == 3) || [data isEmpty]) {
        return data;
    }
    NSData *key = [self generateBlockKey:block];
    return [self translate:data usingKey:key];
}

- (NSData *)translate:(NSData *)data usingKey:(NSData *)key {
    size_t bufferSize = data.length + kCCBlockSizeAES128;
    uint8_t buffer[bufferSize];
    size_t numBytesEncrypted = 0;
    uint8_t *ivPtr = nil;

    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionECBMode, key.bytes, kCCBlockSizeAES128, ivPtr, data.bytes, data.length, buffer, bufferSize, &numBytesEncrypted);

    NSData *clear = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    if (cryptStatus == kCCSuccess) {
        return clear;
    }

    return nil;
}

- (NSData *)obscure:(NSData *)data usingKey:(NSData *)key {
    size_t bufferSize = data.length + kCCBlockSizeAES128;
    uint8_t buffer[bufferSize];
    size_t numBytesEncrypted = 0;
    uint8_t *ivPtr = nil;

    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionECBMode, key.bytes, kCCBlockSizeAES128, ivPtr, data.bytes, data.length, buffer, bufferSize, &numBytesEncrypted);

    NSData *clear = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    if (cryptStatus == kCCSuccess) {
        return clear;
    }

    return nil;
}

- (NSData *)generateBlockKey:(uint8_t)blockNumber {
    uint8_t blockByte = blockNumber;
    uint8_t key[CC_MD5_DIGEST_LENGTH];
    NSMutableData *preKey = [NSMutableData dataWithCapacity:0x56];

    [preKey appendData:[self readBlock:0]];
    [preKey appendData:[self readBlock:1]];
    [preKey appendBytes:&blockByte length:1];
    [preKey appendData:[SUFFIX.rot13 dataUsingEncoding:NSUTF8StringEncoding]];

    CC_MD5(preKey.bytes, (unsigned int)preKey.length, key);
    return [NSData dataWithBytes:key length:CC_MD5_DIGEST_LENGTH];
}

#pragma mark - Class methods

+ (NSArray *)categories {
    return @[ @"character", @"trap", @"item", @"location" ];
}

+ (void)allFilesystem:(void (^)(NSArray *results))successCallback {
    NSPredicate *nonenonePredicate = [NSPredicate predicateWithFormat:@"element != 'none' || type != 'none'"];
    NSMutableArray *tokens = [@[] mutableCopy];

    NSArray *docDirSearch = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:docDirSearch[0] error:nil];
    for (NSString *filepath in filePathsArray) {
        NSString *name = [[filepath lastPathComponent] stringByDeletingPathExtension];

        NSData *data = [[NSFileManager defaultManager] contentsAtPath:filepath];

        Token *t = [[Token alloc] initWithName:name andData:data];
        [tokens addObject:t];
    }

    NSArray *filtered = [[tokens filteredArrayUsingPredicate:nonenonePredicate] sortedArrayUsingSelector:@selector(compare:)];
    if (TOKEN_DEBUG) {
        for (Token *t in [filtered objectEnumerator]) {
            NSLog(@"%@: %@", t.name, t.nickname);
        }
    }
    successCallback(filtered);
}

+ (void)all:(void (^)(NSArray *results))successCallback {
    RKObjectManager *manager = [RKObjectManager sharedManager];
    NSPredicate *nonenonePredicate = [NSPredicate predicateWithFormat:@"element != 'none' || type != 'none'"];
    [manager getObjectsAtPathForRouteNamed:allTokensRoute object:[self class]
        parameters:nil
        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            NSArray *filtered = [[mappingResult.array filteredArrayUsingPredicate:nonenonePredicate] sortedArrayUsingSelector:@selector(compare:)];
            if (TOKEN_DEBUG) {
                for (Token *t in [filtered objectEnumerator]) {
                    NSLog(@"%@: %@", t.name, t.nickname);
                }
            }
            successCallback(filtered);
        }
        failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                       NSLog(@"Pdroblem creating token.  Here is the excuse I got: %@", error);
        }];
}

+ (void)configureRestKit {
    //RKLogLevelTrace
    //RKLogLevelDebug
    RKLogConfigureByName(
        "RestKit/Network", RKLogLevelCritical);
    RKLogConfigureByName(
        "RestKit/ObjectMapping", RKLogLevelCritical);

    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:API_BASE_URL];

    // initialize RestKit
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];

    //Set up routes
    NSString *pathPattern = @"/token/:_id";
    //@"/bettse/b2ef149d803c821ab459/raw/11cb92f884c9912b4f3c0a21c3d9c2726006b9b2/backup.json"

    //All (FOOKIN' RESTKIT)
    [objectManager.router.routeSet addRoute:[RKRoute routeWithName:allTokensRoute pathPattern:@"/token" method:RKRequestMethodGET]];

    //One
    [objectManager.router.routeSet addRoute:[RKRoute routeWithClass:[Token class] pathPattern:pathPattern method:RKRequestMethodGET]];

    //New
    [objectManager.router.routeSet addRoute:[RKRoute routeWithClass:[Token class] pathPattern:@"/token" method:RKRequestMethodPOST]];

    //Update
    [objectManager.router.routeSet addRoute:[RKRoute routeWithClass:[Token class] pathPattern:pathPattern method:RKRequestMethodPUT]];
    //Intentionally no delete
    //Not patch since that's supposed to be a diff and I think i'll always send full data

    /* RESPONSE MAPPING */

    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Token class]];

    NSDictionary *mappingDict = @{
        @"id" : @"_id",
        @"data" : @"data",
        @"element" : @"element",
        @"type" : @"type",
        @"name" : @"name",
    };
    //add the mapping to the manager
    [mapping addAttributeMappingsFromDictionary:mappingDict];

    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                     method:RKRequestMethodAny
                                                pathPattern:nil
                                                    keyPath:nil
                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    [objectManager addResponseDescriptor:responseDescriptor];

    responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                     method:RKRequestMethodAny
                                                pathPattern:nil
                                                    keyPath:nil
                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:responseDescriptor];

    /* REQUEST MAPPING */

    // setup request object mappings
    RKObjectMapping *requestMapping = [mapping inverseMapping];

    RKRequestDescriptor *requestDescriptor =
        [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                              objectClass:[Token class]
                                              rootKeyPath:nil
                                                   method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:requestDescriptor];
}

+ (void)initialize {
    //NSLog(@"Initialize; self = %@", self);
    [self configureRestKit];
}

@end
