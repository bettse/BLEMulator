//
//  NSData+empty.h
//  BlueSim
//
//  Created by Eric Betts on 3/22/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MAX_ZEROS 16

@interface NSData (empty)
- (BOOL)isEmpty;
@end
