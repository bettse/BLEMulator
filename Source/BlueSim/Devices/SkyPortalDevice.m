//
//  SkyPortalDevice.m
//  BlueSim
//
//  Created by Eric Betts on 11/28/14.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "SkyPortalDevice.h"

@implementation SkyPortalDevice

- (id)init {
    self = [super init];
    if (self) {
        sequence = 0;
        self.manager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
        portalSlots = [NSMutableArray arrayWithCapacity:16];
    }
    return self;
}

- (NSString *)name {
    return @"Sky Portal";
}

- (NSString *)localName {
    NSString *nameKey = @"Skylanders Portal\0";
    return nameKey;
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn: {
            self.peripheralManagerPoweredOn = YES;
            if (self.on)
                [self startServices];
            break;
        }
        case CBPeripheralManagerStatePoweredOff: {
            self.peripheralManagerPoweredOn = NO;
            if (self.on)
                [self stopServices];
            break;
        }
        default: {
            NSLog(@"CBPeripheralManager changed state to %d", (int)peripheral.state);
            break;
        }
    }
}

- (void)startServices {
    NSLog(@"Starting the %@ device", self.name);
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    for (CBMutableService *service in self.services) {
        [self.manager addService:service];
    }
}

- (void)stopServices {
    NSLog(@"Stopping the %@ device", self.name);
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [self.manager removeAllServices];
    [self.manager stopAdvertising];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request {
    request.value = [self getCharacteristicValue:request.characteristic];
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
}

- (void)setOn:(BOOL)on {
    if (_on == on)
        return;

    if (self.peripheralManagerPoweredOn) {
        if (on) {
            [self startServices];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenSelected:) name:@"tokenSelected" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(phoneShake:) name:@"shake" object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            for (id i in portalSlots) {
                Token *token = (Token *)i;
                [token save:^{
                    //No op
                }];
            }
            [portalSlots removeAllObjects];
            [self stopServices];
        }
    }
    _on = on;
}

- (NSArray *)services {
    if (!services)
        services = @[ [self createPortalService] ];
    return services;
}

- (CBMutableService *)createPortalService {
    self.portalService = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:PORTAL_SERVICE_UUID] primary:YES];

    self.updateCharacteristic =
        [[CBMutableCharacteristic alloc]
            initWithType:[CBUUID UUIDWithString:UPDATE_CHARACTERISTIC_UUID]
              properties:(CBCharacteristicPropertyNotify | CBCharacteristicPropertyRead)
                   value:nil
             permissions:CBAttributePermissionsReadable];

    self.writeCharacteristic =
        [[CBMutableCharacteristic alloc]
            initWithType:[CBUUID UUIDWithString:WRITE_CHARACTERISTIC_UUID]
              properties:(CBCharacteristicPropertyWrite | CBCharacteristicPropertyWriteWithoutResponse)
                   value:nil
             permissions:CBAttributePermissionsWriteable];

    self.portalService.characteristics = @[ self.updateCharacteristic, self.writeCharacteristic ];

    return self.portalService;
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error {
    if (!error) {
        NSDictionary *advertisingData = @{
            CBAdvertisementDataLocalNameKey : self.localName,
            CBAdvertisementDataServiceUUIDsKey : @[ [CBUUID UUIDWithString:PORTAL_SERVICE_UUID_SHORT] ],
            CBAdvertisementDataIsConnectable : [NSNumber numberWithBool:YES]
        };
        //NSLog(@"Advertising: %@", advertisingData);
        [self.manager startAdvertising:advertisingData];
    }
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
    if (error) {
        NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
    }
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic {
    NSLog(@"Unsubscribed");
}

//Called by didReceiveReadRequest in parent class
- (NSData *)getCharacteristicValue:(CBCharacteristic *)characteristic {
    if ([characteristic.UUID isEqual:self.updateCharacteristic.UUID]) {
        //NSLog(@"Received read request");
        return [self statusResponse];
    }
    return nil;
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic {
    NSLog(@"Subscribed %@", characteristic.UUID);
    if ([characteristic.UUID isEqual:self.updateCharacteristic.UUID]) {
        //[self sendUpdate];
    }
}

- (NSData *)statusResponse {
    //Byte[0] = 'S' for status
    //Byte[1,2,3,4] = tokens present/transitioning using 2 bits per token.  even bit for token presence, odd bit for if token is transitioning to other state
    //Byte[5] = sequence
    uint8_t bytes[BLE_SIZE] = {'S', 00, 00, 00, 00, 0xFF, 0x01, 0xaa, 0x86, 0x02, 0x19, 0x00, 0x00, 0x00, 0x00};
    
    bytes[1] = portalSlots.count;
    bytes[5] = sequence++ % 0xFF;
    NSData *payload = [NSData dataWithBytes:bytes length:BLE_SIZE];
    return payload;
}

- (void)respondTo:(NSData *)data {
    NSLog(@"%c %@", ((char*)data.bytes)[0], [data subdataWithRange:NSMakeRange(1, BLE_SIZE-1)]);
    const uint8_t *reportData = data.bytes;
    uint8_t response[BLE_SIZE] = {0};
    NSData *responseData;

    switch (reportData[0]) {
        case 'A':
            response[0] = reportData[0];
            response[1] = reportData[1];
            
            response[2] = 0x62;
            response[3] = 0x02;
            response[4] = 0x19;
            response[5] = 0xaa;
            
            response[6] = 0x01;
            response[7] = 0x5e; //0x53;
            response[8] = 0x49; //0xbc;
            response[9] = 0x53; //0x58;
            response[10] = 0xbb; //0xfc;
            response[11] = 0x35; //0x7d;
            response[12] = 0xc6; //0xf4;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'B':
            response[0] = reportData[0];
            response[1] = 0x00;
            response[2] = 0x00;
            response[3] = 0x00;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'C': //Color
            //NSLog(@"Color: %@", [status componentsJoinedByString:@" "]);
            break;
        case 'D':
            response[0] = reportData[0];
            response[1] = 0x00;
            response[2] = 0x00;
            response[3] = 0x00;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'Q': //Query
            
            response[0] = reportData[0];
            response[1] = reportData[1];
            response[2] = reportData[2];
            responseData = [NSData dataWithBytes:response length:3];
            if ([portalSlots objectAtIndex:(reportData[1] & 0x0f)]) { //Cheating so I can declare some objects
                int blockNumber = reportData[2];
                NSMutableData *temp = responseData.mutableCopy;
                
                NSData *blockData;
                NSString *ro = @"0000000000000F0F0F69000000000000";
                NSString *rw = @"0000000000007F0F0869000000000000";
                if (blockNumber == 3) {
                    blockData = [ro hexData];
                } else if (blockNumber % 4 == 3) {
                    blockData = [rw hexData];
                } else {
                    blockData = [(Token*)(portalSlots[reportData[1] & 0x0f]) readBlock:blockNumber];
                }
                                
                [temp appendData:blockData];
                responseData = [NSData dataWithData:temp];
                NSLog(@"%c index:%x block:%02u %@", reportData[0], reportData[1] & 0x0f, reportData[2], blockData);
            }
            break;
        case 'S': //Status
            responseData = [self statusResponse];
            break;
        case 'J': //???
            response[0] = reportData[0];
            response[1] = 0x00;
            response[2] = 0x00;
            response[3] = 0x00;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'K':
            response[0] = reportData[0];
            response[1] = 0x00;
            response[2] = 0x00;
            break;
        case 'L': //Light up trap LED
            break;
        case 'R':
            response[0] = reportData[0];
            response[1] = 0x02;
            response[2] = 0x19;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'W': //Write
            [self writeToken:reportData[1] block:reportData[2] withData:[data subdataWithRange:NSMakeRange(3, BLOCK_SIZE)]];
            response[0] = reportData[0];
            response[1] = reportData[1];
            response[2] = reportData[2];
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        case 'T':
            response[0] = reportData[0];
            response[1] = 0x00; //1?
            response[2] = 0x00;
            response[3] = 0x00;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
        default:
            NSLog(@"Unhandled command %c", reportData[0]);
            response[0] = 0x00;
            responseData = [NSData dataWithBytes:response length:BLE_SIZE];
            break;
    }

    if (responseData) {
        NSLog(@"\t=> %@", responseData);
        [self.manager updateValue:responseData forCharacteristic:self.updateCharacteristic onSubscribedCentrals:nil];
    }
}


- (void)writeToken:(uint8_t)index block:(uint8_t)block withData:(NSData *)data {
    int arrayIndex = index & 0x0f;
    if (portalSlots[arrayIndex]) {
        [portalSlots[arrayIndex] writeBlock:block withData:data];
    }
}

- (void)tokenSelected:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    Token *t = userInfo[@"token"];
    
    if ([portalSlots containsObject:t]) {
        NSLog(@"Unloading %@", t.name);
        [t save:^{
            [portalSlots removeObject:t];
        }];
    } else {
        NSLog(@"Loading %@ (%@)", t.name, t.category);
        [portalSlots addObject:t];
    }
}

- (void)phoneShake:(NSNotification *)notification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    /*
    Token *characterToken = portalSlots[@"character"];
    if (characterToken) {
        [characterToken setMoney:(characterToken.money + 1000)];
    }
     */
}

- (void)clearTrap {
    //[portalSlots removeObjectForKey:@"trap"];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests {
    for (CBATTRequest *request in requests) {
        [self respondTo:request.value];
    }
}

- (void)centralReceived:(NSData *)data {
    const uint8_t *bytes = [data bytes];
    if (self.updateCharacteristic) {
        NSLog(@"=> %c - %@", bytes[0], data);
        [self.manager updateValue:data forCharacteristic:self.updateCharacteristic onSubscribedCentrals:nil];
    }
}

@end
