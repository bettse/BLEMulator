//
//  SkyPortalDevice.m
//  BlueSim
//
//  Created by Eric Betts on 11/28/14.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "PortalClient.h"

@implementation PortalClient

- (id)init {
    self = [super init];
    if (self) {
        centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        exportToken = nil;
    }
    return self;
}

static inline BOOL check_bit(unsigned int x, int bitNum) {
    return x & (1L << bitNum);
}

- (void)statusRequest {
    uint8_t bytes[BLE_SIZE] = {0};
    bytes[0] = 'S';
    if (remotePeripheral) {
        [remotePeripheral writeValue:[NSData dataWithBytes:bytes length:BLE_SIZE] forCharacteristic:write type:CBCharacteristicWriteWithResponse];
    }
}

- (void)tokenSelected:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    exportToken = userInfo[@"token"];
    NSLog(@"Set export token to %@", exportToken);
}

- (void)centralReceived:(NSData *)data {
    const uint8_t *reportData = data.bytes;
    int newTokenIndex = -1;
    bool dumpToken = true;
    NSLog(@"<= %c [%@]", reportData[0], data);

    switch (reportData[0]) {
        case 'S':
            newTokenIndex = [self decodeStatus:data];
            if (exportToken && newTokenIndex > -1) {
                [self readBlock:0 ofToken:newTokenIndex];
            } else if (dumpToken && newTokenIndex > -1) {
                incomingToken = [NSMutableData dataWithCapacity:TOKEN_SIZE];
                NSLog(@"New token detected[%i], requesting block 0", newTokenIndex);
                [self readBlock:0 ofToken:newTokenIndex];
            } else if (newTokenIndex > -1) {
                NSLog(@"S, new token, not setup to dump or export");
            }
            
            break;
        case 'Q':
            if (dumpToken && reportData[1] > 0x0F) {
                uint8_t tokenIndex = reportData[1] & 0x0f;
                uint8_t block = reportData[2];
                NSData *newBlock = [data subdataWithRange:NSMakeRange(3, BLOCK_SIZE)];
                [incomingToken appendData:newBlock];
                NSDictionary *userInfo = @{
                    @"tokenSize" : [NSNumber numberWithInteger:incomingToken.length],
                    @"tokenIndex" : [NSNumber numberWithInteger:tokenIndex],
                    @"blockNumber" : [NSNumber numberWithInteger:block]
                };
                [[NSNotificationCenter defaultCenter] postNotificationName:@"blockRead" object:self userInfo:userInfo];
                if (incomingToken.length < BLOCK_SIZE * 16) {
                    NSLog(@"%lu bytes of new token, requesting block %i", incomingToken.length, block + 1);
                    [self readBlock:(block + 1)ofToken:tokenIndex];
                } else if (incomingToken.length == BLOCK_SIZE * 16) {
                    // Save token/ post to web
                    NSString *type = [[incomingToken subdataWithRange:NSMakeRange(16, 2)] hexadecimalString];
                    NSString *variant = [[incomingToken subdataWithRange:NSMakeRange(28, 2)] hexadecimalString];
                    NSString *name = [NSString stringWithFormat:@"Scanned Token %@-%@", type, variant];
                    Token *t = [[Token alloc] initWithName:name andData:incomingToken];
                    NSLog(@"%lu bytes of new token[%@], saving.", incomingToken.length, t.name);
                    [t save:^{
                        NSLog(@"Uploaded %@", t.name);
                    }];
                }
            } else if (reportData[2] == 0) { //Reasponse to [read block 0] command
                NSLog(@"Got response to block0");

                uint8_t tokenIndex = reportData[1] & 0x0f;
                NSMutableData *block0 = [[data subdataWithRange:NSMakeRange(3, BLOCK_SIZE)] mutableCopy];
                [block0 appendData:[[exportToken readBlock:1] subdataWithRange:NSMakeRange(0, BLOCK_SIZE - 2)]];
                unsigned short chk = [block0 crc16Checksum];
                NSData *calculatedChecksum = [NSData dataWithBytes:&chk length:2];
                [block0 appendData:calculatedChecksum];

                NSData *block1 = [block0 subdataWithRange:NSMakeRange(BLOCK_SIZE, BLOCK_SIZE)];

                [self writeBlock:1 ofToken:tokenIndex withData:block1];
            }
            break;
        case 'W': //Write response
            NSLog(@"W response %u %u", reportData[1] & 0x0f, reportData[2]);
            break;
    }
}

- (void)readBlock:(uint8_t)block ofToken:(uint8_t)token {
    uint8_t response[BLE_SIZE] = {0};
    response[0] = 'Q';
    response[1] = token;
    response[2] = block;
    [remotePeripheral writeValue:[NSData dataWithBytes:response length:BLE_SIZE] forCharacteristic:write type:CBCharacteristicWriteWithResponse];
}

- (void)writeBlock:(uint8_t)blockIndex ofToken:(uint8_t)tokenIndex {
    NSData *tokenBlock = [exportToken readBlock:blockIndex];
    [self writeBlock:blockIndex ofToken:tokenIndex withData:tokenBlock];
}

- (void)writeBlock:(uint8_t)blockIndex ofToken:(uint8_t)tokenIndex withData:(NSData *)newData {
    uint8_t response[BLE_SIZE] = {0};
    response[0] = 'W';
    response[1] = tokenIndex;
    response[2] = blockIndex;
    if (blockIndex % 4 == 3 || blockIndex >= BLOCK_COUNT) { //Skip trailers and invalid indexes
        return;
    }

    [newData getBytes:response + 3 length:BLOCK_SIZE]; //write block into response array
    NSLog(@"writeBlock: %u %@", blockIndex, [NSData dataWithBytes:response length:BLE_SIZE]);
    [remotePeripheral writeValue:[NSData dataWithBytes:response length:BLE_SIZE] forCharacteristic:write type:CBCharacteristicWriteWithResponse];
}

- (int)decodeStatus:(NSData *)data {
    unsigned int statusBitVector = 0;
    [data getBytes:&statusBitVector range:NSMakeRange(1, 4)];
    for (uint8_t index = 0; index < 16; index++) {
        if (check_bit(statusBitVector, index * 2) && check_bit(statusBitVector, index * 2 + 1)) {
            return index; //return first found
        }
    }
    return -1;
}

#pragma mark - Stuff to act as a central

- (void)startScan {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    NSArray *services = @[ [CBUUID UUIDWithString:PORTAL_SERVICE_UUID_SHORT] ];
    [centralManager scanForPeripheralsWithServices:services options:nil];
}

- (void)stopScan {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    [centralManager stopScan];
}

- (void)disconnectPortal {
    if (remotePeripheral) {
        [centralManager cancelPeripheralConnection:remotePeripheral];
    }
}

/*
 Uses CBCentralManager to check whether the current platform/hardware supports Bluetooth LE. An alert is raised if Bluetooth LE is not enabled or is not supported.
 */
- (BOOL)isLECapableHardware {
    NSString *state = nil;

    switch ([centralManager state]) {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            return TRUE;
        case CBCentralManagerStateUnknown:
        default:
            return FALSE;
    }

    NSLog(@"Central manager state: %@", state);
    return FALSE;
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    [self isLECapableHardware];
}

/*
 Invoked when the central discovers peripheral while scanning.
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    //NSLog(@"peripheral discovered");
    remotePeripheral = aPeripheral;
    [centralManager connectPeripheral:remotePeripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:YES]}];
}

/*
 Invoked whenever a connection is succesfully created with the peripheral.
 Discover available services on the peripheral
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral {
    //NSLog(@"peripheral connected");
    [self stopScan];
    [aPeripheral setDelegate:self];
    [aPeripheral discoverServices:nil];
    keepAlive = [NSTimer scheduledTimerWithTimeInterval:KEEPALIVE_INTERVAL target:self selector:@selector(statusRequest) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"portalConnected" object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenSelected:) name:@"tokenSelected" object:nil];
}

/*
 Invoked whenever an existing connection with the peripheral is torn down.
 Reset local variables
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    //NSLog(@"peripheral disconnected");
    if (remotePeripheral) {
        [remotePeripheral setDelegate:nil];
        remotePeripheral = nil;
        if (keepAlive) {
            [keepAlive invalidate];
            keepAlive = nil;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"portalDisconnected" object:self];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"tokenSelected" object:nil];
    }
}

/*
 Invoked whenever the central manager fails to create a connection with the
 peripheral.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    NSLog(@"Fail to connect to peripheral: %@ with error = %@", aPeripheral, [error localizedDescription]);
}

#pragma mark - CBPeripheral delegate methods
/*
 Invoked upon completion of a -[discoverServices:] request.
 Discover available characteristics on interested services
 */
- (void)peripheral:(CBPeripheral *)aPeripheral
    didDiscoverServices:(NSError *)error {
    //NSLog(@"Peripheral found %@", aPeripheral);
    for (CBService *aService in aPeripheral.services) {
        // NSLog(@"Service found %@", aService);
        [aPeripheral discoverCharacteristics:nil forService:aService];
    }
}

/*
 Invoked upon completion of a -[discoverCharacteristics:forService:] request.
 Perform appropriate operations on interested characteristics
 */
- (void)peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    for (CBCharacteristic *aChar in service.characteristics) {
        if ((aChar.properties & CBCharacteristicPropertyRead) == CBCharacteristicPropertyRead &&
            (aChar.properties & CBCharacteristicPropertyNotify) == CBCharacteristicPropertyNotify) {
            // NSLog(@"Subscribing to characteristic %@ of service %@", aChar.UUID,
            // service.UUID);
            [remotePeripheral setNotifyValue:YES forCharacteristic:aChar];
        } else if ((aChar.properties & CBCharacteristicPropertyWrite) == CBCharacteristicPropertyWrite) {
            write = aChar;
        }
    }
    

}

- (void)peripheral:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"%s: error: %@", __PRETTY_FUNCTION__, error);
    }
}

/*
 Invoked upon completion of a -[readValueForCharacteristic:] request or on the
 reception of a notification/indication.
 */
- (void)peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if ((characteristic.value) || !error) {
        [self centralReceived:characteristic.value];
    }
    if (error) {
        NSLog(@"Error: %@", error);
    }
}

@end
