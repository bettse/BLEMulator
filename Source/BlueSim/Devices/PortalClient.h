//
//  SkyPortalDevice.h
//  BlueSim
//
//  Created by Eric Betts on 11/28/14.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Token.h"

#define PORTAL_SERVICE_UUID_SHORT @"1530"

#define BLOCK_SIZE 16
#define BLE_SIZE 20
#define KEEPALIVE_INTERVAL 5
#define BLOCK_COUNT 64

@interface PortalClient : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate> {
    CBCentralManager *centralManager;
    CBPeripheral *remotePeripheral;
    CBCharacteristic *write;

    NSMutableData *incomingToken;
    NSTimer *keepAlive;
    NSMutableArray *tokens;
    Token *exportToken;
}

- (void)startScan;
- (void)disconnectPortal;

@end
