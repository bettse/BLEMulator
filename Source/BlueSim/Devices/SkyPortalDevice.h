//
//  SkyPortalDevice.h
//  BlueSim
//
//  Created by Eric Betts on 11/28/14.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "Token.h"
#import <RestKit/RestKit.h>

#define PORTAL_DEVICE_UUID @"3386CBF3-DAC4-4F49-9D67-AA10B02A9453"
#define PORTAL_SERVICE_UUID_SHORT @"1530"
#define PORTAL_SERVICE_UUID @"533E1530-3ABE-F33F-CD00-594E8B0A8EA3"
#define UPDATE_CHARACTERISTIC_UUID @"533E1542-3ABE-F33F-CD00-594E8B0A8EA3"
#define WRITE_CHARACTERISTIC_UUID @"533E1543-3ABE-F33F-CD00-594E8B0A8EA3"
#define UPDATE_RATE 1
#define BLOCK_SIZE 16
#define BLE_SIZE 20

@interface SkyPortalDevice : NSObject <CBPeripheralManagerDelegate> {
    unsigned int sequence;
    NSArray *services;

    NSString *name;
    NSString *imageName;
    NSString *localName;
    NSMutableArray *portalSlots;
}

@property (nonatomic) BOOL on;

@property (nonatomic, strong) CBPeripheralManager *manager;
@property (nonatomic) BOOL peripheralManagerPoweredOn;

@property (nonatomic, strong) CBMutableService *portalService;
@property (nonatomic, strong) CBMutableCharacteristic *updateCharacteristic;
@property (nonatomic, strong) CBMutableCharacteristic *writeCharacteristic;
@property (nonatomic, strong) NSTimer *timer;

- (NSData *)getCharacteristicValue:(CBCharacteristic *)characteristic;

@end
