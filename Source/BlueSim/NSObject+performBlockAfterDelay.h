//
//  NSObject+performBlockAfterDelay.h
//  BlueSim
//
//  Created by Eric Betts on 2/5/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (performBlockAfterDelay)

- (void)performBlock:(dispatch_block_t)block afterDelay:(NSTimeInterval)delay;

@end
