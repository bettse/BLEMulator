//
//  VirtualPortalViewController.h
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkyPortalDevice.h"
#import "Token.h"

@interface VirtualPortalViewController : UIViewController <UICollectionViewDataSource, UIGestureRecognizerDelegate> {
    SkyPortalDevice *skyPortalDevice;
    NSArray *availableTokens;
    NSArray *visibleTokens;
    NSInteger tokenIndex;
}

@property (weak, nonatomic) IBOutlet UISwitch *powerSwitch;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
