//
//  DataExplorerViewController.m
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "DataExplorerViewController.h"

@interface DataExplorerViewController ()

@end

@implementation DataExplorerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Connect data
    //self.testDataPicker.dataSource = self;
    //self.testDataPicker.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)step:(id)sender {
    UIStepper *step = (UIStepper *)sender;
    uint8_t newByte = (uint8_t)step.value;
    self.byteValue.text = [NSString stringWithFormat:@"%02x", newByte];
    variant = newByte;
}

- (IBAction)changeByte:(id)sender {
    UISlider *slider = (UISlider *)sender;
    uint8_t newByte = (uint8_t)slider.value;
    self.byteValue.text = [NSString stringWithFormat:@"%02x", newByte];
    variant = newByte;
}

- (IBAction)addCustom:(id)sender {
    NSString *base = @"afbee9ef1781010fc407000000000014d800000000000000000000000030ffff";
    uint8_t variant_byte = (uint8_t)variant;
    NSString *typename = [NSString stringWithFormat:@"%li", variant];
    NSMutableData *tagData = [[base hexData] mutableCopy];
    [tagData replaceBytesInRange:NSMakeRange(28, 1) withBytes:&variant_byte];
    self.tokenBlocks.text = [NSString stringWithFormat:@"%@", tagData];

    Token *custom = [[Token alloc] initWithName:typename andData:tagData];
    custom.type = @"trap";

    [[NSNotificationCenter defaultCenter] postNotificationName:@"tokenSelected" object:self userInfo:@{ @"token" : custom }];

    //Prompt user for name
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:typename message:@"Assign token name" preferredStyle:UIAlertControllerStyleAlert];

    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = typename;
        self.tokenName = textField;
    }];

    UIAlertAction *saveName = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //Do some thing here
        custom.name = self.tokenName.text;
        //[custom save];
        [alert dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clearTrap" object:self];
    }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clearTrap" object:self];
    }];

    [alert addAction:saveName];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
