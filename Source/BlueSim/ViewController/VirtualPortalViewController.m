//
//  VirtualPortalViewController.m
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "VirtualPortalViewController.h"

@implementation VirtualPortalViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    skyPortalDevice = [SkyPortalDevice new];
    self.powerSwitch.on = skyPortalDevice.on;

    availableTokens = [NSMutableArray arrayWithCapacity:5];

    // Connect data
    [Token all:^(NSArray *results) {
        availableTokens = results;
        [self.collectionView reloadData];
    }];

    // attach long press gesture to collectionView
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = .5; //seconds
    lpgr.delegate = self;
    [self.collectionView addGestureRecognizer:lpgr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)powerSwitchChanged:(id)sender {
    //NSLog(@"%s Device: %i UI: %i", __PRETTY_FUNCTION__, skyPortalDevice.on, self.powerSwitch.on);
    if (skyPortalDevice.on != self.powerSwitch.on) {
        if (skyPortalDevice.on) {
            [Token all:^(NSArray *results) {
                availableTokens = results;
                [self.collectionView reloadData];
            }];
        }
        skyPortalDevice.on = self.powerSwitch.on;
    }
}

#pragma mark -
#pragma mark uicollectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return availableTokens.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TokenCell" forIndexPath:indexPath];
    UILabel *name = (UILabel *)[cell viewWithTag:2];
    Token *t = availableTokens[indexPath.row];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:t.name];
    if (t.color) {
        [attrStr addAttribute:NSBackgroundColorAttributeName value:t.color range:NSMakeRange(0, t.name.length)];
        //TODO: Generalize to a nice complementary background color
        if ([t.element isEqualToString:@"dark"] || [t.element isEqualToString:@"earth"]) {
            [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, t.name.length)];
        }
    }
    name.attributedText = attrStr;

    UIImageView *cellImage = (UIImageView *)[cell viewWithTag:1];
    cellImage.image = t.image;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Token *t = availableTokens[indexPath.row];
    NSLog(@"Selected %@", t);
    [t dump];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tokenSelected" object:self userInfo:@{ @"token" : t }];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.collectionView];

    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        NSLog(@"couldn't find index path");
    } else {
        //UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        Token *t = availableTokens[indexPath.row];
        [self confirmReset:t];
    }
}

- (void)confirmReset:(Token *)token {
    //Prompt user for name
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:token.name message:@"Confirm the reset of this token" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [token reset];
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];

    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shake" object:self userInfo:nil];
    }
}

@end
