//
//  DataExplorerViewController.h
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Token.h"

@interface DataExplorerViewController : UIViewController <UITextFieldDelegate> {
    NSInteger variant;
}

@property (weak, nonatomic) IBOutlet UITextField *byteValue;
@property (weak, nonatomic) IBOutlet UISlider *byteSlider;
@property (weak, nonatomic) IBOutlet UILabel *tokenBlocks;
@property (weak, nonatomic) IBOutlet UITextField *tokenName;

@end
