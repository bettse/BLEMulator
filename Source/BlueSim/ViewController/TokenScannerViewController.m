//
//  TokenScannerViewController.m
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "TokenScannerViewController.h"

@implementation TokenScannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    portalClient = [PortalClient new];
    // http http://displaycase.ericbetts.org/token  | jq -r '.[] | select(.type=="none") | select(.element=="none")'
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(portalConnected) name:@"portalConnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(portalDisconnected) name:@"portalDisconnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blockRead:) name:@"blockRead" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Note: Only 2 blocks are needed to reproduce token
- (void)blockRead:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    long tokenIndex = [(NSNumber *)userInfo[@"tokenIndex"] integerValue];
    long blockNumber = [(NSNumber *)userInfo[@"blockNumber"] integerValue];

    self.output.text = [NSString stringWithFormat:@"%@\nToken #%lu\tblock %lu", self.output.text, tokenIndex, blockNumber];
}

- (IBAction)connectRealPortal:(id)sender {
    //NSLog(@"%s %i", __PRETTY_FUNCTION__, self.connectPortal.selected);
    if (self.connectPortal.selected) {
        [portalClient disconnectPortal];
    } else {
        [portalClient startScan];
    }
}

- (void)portalConnected {
    self.connectPortal.selected = YES;
}

- (void)portalDisconnected {
    self.connectPortal.selected = NO;
}

@end
