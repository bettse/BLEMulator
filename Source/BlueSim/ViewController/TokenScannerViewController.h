//
//  TokenScannerViewController.h
//  BlueSim
//
//  Created by Eric Betts on 1/29/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortalClient.h"
#import "Token.h"

#define CONNECT @"Connect"
#define DISCONNECT @"Disconnect"
#define CONNECTING @"Connecting..."
#define DISCONNECTING @"Disconnecting..."

@interface TokenScannerViewController : UIViewController {
    PortalClient *portalClient;
}

@property (weak, nonatomic) IBOutlet UIButton *connectPortal;
@property (weak, nonatomic) IBOutlet UIProgressView *readProgress;
@property (weak, nonatomic) IBOutlet UITextView *output;

@end
