//
//  Token.h
//  BlueSim
//
//  Created by Eric Betts on 12/18/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "NSData+Conversion.h"
#import "NSData+crc16Checksum.h"
#import "NSData+empty.h"
#import "NSString+dataFromHex.h"
#import "NSObject+performBlockAfterDelay.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSString+rot13.h"

#define API_BASE_URL @"http://displaycase.ericbetts.org"
//#define API_BASE_URL @"https://cdn.rawgit.com"
//#define API_BASE_URL @"http://ataraxia.ericbetts.org:1337"

#define BLOCK_SIZE 16
#define BLOCK_COUNT 64
#define TOKEN_SIZE 1024
#define TOKEN_DEBUG false

#define SUFFIX @" Pbclevtug (P) 2010 Npgvivfvba. Nyy Evtugf Erfreirq. "

//Havne't checked bounds
#define CHARACTER_RANGE NSMakeRange(0, 100)
#define GIANT_RANGE NSMakeRange(100, 100)
#define ITEM_RANGE NSMakeRange(200, 100)
#define TRAP_RANGE NSMakeRange(210, 10)
#define EXPANSION_RANGE NSMakeRange(300, 100)
#define LEGENDARY_RANGE NSMakeRange(400, 50)
#define TRAPTEAM_RANGE NSMakeRange(450, 50)
#define MINI_RANGE NSMakeRange(500, 50)
#define SWAPFORCE_RANGE NSMakeRange(3000, 15)

#define CHARACTER_CATID 0
#define TRAP_CATID 1
#define ITEM_CATID 2
#define LOCATION_CATID 3

@interface Token : NSObject {
    NSMutableData *encodeddata;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *element;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *_id;

@property (nonatomic, strong, readonly) UIImage *image;

+ (void)all:(void (^)(NSArray *results))success;
+ (void)configureRestKit;
+ (NSArray *)categories;

- (Token *)initWithName:(NSString *)name andData:(NSData *)data;
- (NSData *)readBlock:(NSInteger)block;
- (void)writeBlock:(NSInteger)block withData:(NSData *)newData;
- (void)save:(void (^)(void))successCallback;
- (void)dump;
- (void)reset;

//Read
- (uint16_t)typeId;
- (NSUInteger)exp;
- (uint16_t)money;
- (UIColor *)color;
- (NSString *)category;
- (NSUInteger)categoryId;
- (NSData *)translate:(NSInteger)block;

//Write
- (void)setMoney:(uint16_t)value;

@end
