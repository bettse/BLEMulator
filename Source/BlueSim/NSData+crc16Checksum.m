//
//  NSData+crc16Checksum.m
//  BlueSim
//
//  Created by Eric Betts on 12/20/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "NSData+crc16Checksum.h"

@implementation NSData (crc16Checksum)

- (unsigned short)crc16Checksum
{
    unsigned int crc;
    crc = 0xFFFF;
    unsigned long length = self.length;
    const uint8_t *bytes = [self bytes];
    
    
    //http://stackoverflow.com/questions/21252069/ccitt-crc-16-bit-start-value-0xffff
    while (length--) {
        crc ^= *bytes++ << 8;
        for (int i = 0; i < 8; i++) {
            crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
        }
    }
    return crc & 0xffff;
    
}

@end
