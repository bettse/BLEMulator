//
//  NSObject+performBlockAfterDelay.m
//  BlueSim
//
//  Created by Eric Betts on 2/5/15.
//  Copyright (c) 2015 Damien Guard. All rights reserved.
//

#import "NSObject+performBlockAfterDelay.h"

@implementation NSObject (performBlockAfterDelay)

- (void)performBlock:(dispatch_block_t)block afterDelay:(NSTimeInterval)delay;
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_current_queue(), block);
}

@end
